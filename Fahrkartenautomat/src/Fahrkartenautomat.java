﻿import java.util.Scanner;
class Fahrkartenautomat {
    public static void main(String[] args) {
      double zuZahlenderBetrag;
      double rückgabeBetrag; 
      zuZahlenderBetrag=fahrkartenbestellungErfassen();
      rückgabeBetrag=fahrkartenBezahlen(zuZahlenderBetrag);
      fahrkartenAusgeben();
      rueckgeldAusgeben(rückgabeBetrag);
      main(null);
    }
 
    public static double fahrkartenBezahlen(double d) {
    	double einzahlung=0,eingeworfeneMünze;
    	Scanner tastatur = new Scanner(System.in);
         while(einzahlung < d)
         {
      	   System.out.printf( "Noch zu zahlen: %.2f Euro\n", (d - einzahlung));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur.nextDouble();
           einzahlung += eingeworfeneMünze;
         }
         if (einzahlung>d)
        	 return einzahlung-d;

    	return 0.0;
    }
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
     	    
           while(rückgabebetrag >= 1.99) {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
	          }
     	   if(rückgabebetrag >= 0.99) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            if(rückgabebetrag >= 0.49) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.19) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            if(rückgabebetrag >= 0.09) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            if(rückgabebetrag >= 0.01)// 5 CENT-Münzen
            {
              System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine angenehme Fahrt.\n\n");
           
        }    
    }
    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
     }
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	byte ticketwahl; 
        byte anzahl=0;
        double preis=0;
        boolean gueltigerpreis=false;
        while(gueltigerpreis==false) {
        	String[] bez= {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
        				   "Einzelfahrschein Berlin ABC", "Kurzstrecke",
        				   "Tageskarte Berlin AB", "Tageskarte Berlin BC",
        				   "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
        				   "Kleingruppen-Tageskarte Berlin BC",
        				   "Kleingruppen-Tageskarte Berlin ABC",
        	};
        	double[] preise= {2.9 ,3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9};// Vorteil der Arraydeklaration ist die schnelle zukünftige Änderung der Werte.
        	// Nachteil der neuen Implementation ist die möglicherweise falsche Preis-Bezeichnung Zuweisung bei Änderungen.
        	System.out.println("Wählen Sie Ihr Wunschticket für Berlin AB aus:");
        	for(int i=0; i< bez.length;i++) {
        		System.out.println(bez[i] + " " + preise[i] + " (" +(i+1) +")");
        	}
        	ticketwahl = tastatur.nextByte();
        	
        	if (ticketwahl>0&&ticketwahl<11) {
        	preis=preise[ticketwahl-1];
        	gueltigerpreis=true;
        	}
        }
	        System.out.print("Anzahl Fahrkarten:");
	        anzahl= tastatur.nextByte();
	        while(anzahl<1||anzahl>10) {
	        	System.out.print("Anzahl Fahrkarten:");
		        anzahl= tastatur.nextByte();
	        }
        preis=anzahl*preis;
        return preis;
    }
}