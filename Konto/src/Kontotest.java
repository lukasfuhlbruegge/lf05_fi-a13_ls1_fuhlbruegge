
public class Kontotest {

	public static void main(String[] args) {
		Besitzer b1= new Besitzer();
		b1.getKonto()[0].setIban("DE01 1899 9881 9991 1972 53");
		b1.getKonto()[0].setKontonummer(14159265358979L);
		b1.getKonto()[1].setIban("DE11 2233 4455 6677 8899 00");
		b1.getKonto()[1].setKontonummer(1234567891234560L);
		b1.getKonto()[0].geldEinzahlen(12);
		b1.getKonto()[1].geldEinzahlen(34.6);
		b1.setVorname("Peter");
		b1.setName("Sternberg");
		b1.gesamtUebersicht();
		b1.gesamtGeld();
		b1.getKonto()[0].geldAbheben(3.51);
		b1.getKonto()[0].geldUeberweisen(b1.getKonto()[1], 4.32);
		b1.gesamtUebersicht();
		System.out.println("Der Vorname ist " + b1.getVorname()+ ", der Nachname ist " + b1.getName() + ".");
	}
}
