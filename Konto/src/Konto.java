
public class Konto {
	//Attribute
	private String iban;
	private long kontonummer;
	private double kontostand;
	
	//Konstruktor
	public Konto() {
		
	}
	
	//Verwaltungsmethoden
		public String getIban() {
			return iban;
		}

		public void setIban(String iban) {
			this.iban = iban;
		}

		public long getKontonummer() {
			return kontonummer;
		}

		public void setKontonummer(long kontonummer) {
			this.kontonummer = kontonummer;
		}

		public double getKontostand() {
			return kontostand;
		}

		public void setKontostand(double kontostand) {
			this.kontostand = kontostand;
		}
	//Allgemeine Methoden
	public void geldAbheben(double betrag) {
		kontostand=kontostand-betrag;
	}
	
	public void geldEinzahlen(double betrag) {
		kontostand=kontostand+betrag;
	}
	
	public void geldUeberweisen(Konto k, double betrag) {
		k.setKontostand(k.getKontostand()+betrag);
		setKontostand(getKontostand()-betrag);
	}
	

}
