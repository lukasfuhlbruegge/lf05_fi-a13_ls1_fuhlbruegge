
public class Besitzer {
	// Attribute
	private String name;
	private String vorname;
	private Konto[] konto = new Konto[2];
	//Konstruktor
	public Besitzer() {
		konto= new Konto[2];
		konto[0]= new Konto();
		konto[1]= new Konto();
	}
	//Verwaltungsmethoden
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public Konto[] getKonto() {
		return konto;
	}
//allgemeine Methoden
	public void setKonto(Konto[] konto) {
		this.konto = konto;
	}
	public void gesamtGeld() {
		double kontoStandGesamt = konto[0].getKontostand()+konto[1].getKontostand();	
		System.out.println("Ihr gesamtes Verm�gen betr�gt: " + kontoStandGesamt + "�.");
	}

	public void gesamtUebersicht() {
	System.out.println("Ihr Konto mit der IBAN " + konto[0].getIban() +
			" und der Kontonummer " + konto[0].getKontonummer() +
			" hat einen Kontostand von " + konto[0].getKontostand() + "�." );
	System.out.println("Ihr Konto mit der IBAN " + konto[1].getIban() +
			" und der Kontonummer " + konto[1].getKontonummer() +
			" hat einen Kontostand von " + konto[1].getKontostand() + "�." );
	}

	
	
}


