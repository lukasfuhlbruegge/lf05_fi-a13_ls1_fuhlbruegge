/**
 * Datei: BroadcastKommunikator.java 
 * Aufgabe: Die Klasse dokumentiert die
 * Nachrichten, die Raumschiffe an alle senden.
 * 
 * @author LukasFuhlbruegge
 * @version 1.0
 * 
 */
public class BroadcastKommunikator {
	// Attribute
	private String log;

	// Konstruktor
	public BroadcastKommunikator() {
		log = "LOGBUCH";
	}

	// Verwaltungsmethoden
	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	// allgemeine Methoden
	/**
	 * Effekt: Der String wird im Logbuch gespeichert.
	 * 
	 * @param s Text, der gespeichert werden soll.
	 */
	public void add(String s) {
		log = (log + "\n" + s);
	}

}
