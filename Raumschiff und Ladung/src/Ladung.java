/**
 * Datei: Ladung.java
 * Aufgabe: Die Klasse modelliert eine Ladung eines Raumschiffs.
 * 
 * @author LukasFuhlbruegge
 * @version 1.0
 * 
 */
public class Ladung {

	//Attribute	
	private String typ;
	private int anzahl;
	
	// Konstruktoren
	public Ladung() {
		
	}
	
	public Ladung(String typ, int anzahl) {
		this.typ=typ;
		this.anzahl=anzahl;
	}
	
	//Verwaltungsmethoden
	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	@Override
	public String toString() {
		return ("Ladung: " + getTyp() + " Anzahl: " + getAnzahl());
	}
}
