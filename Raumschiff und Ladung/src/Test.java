/**
 * Datei: Test.java
 * Aufgabe: Die Klasse testet die Klassen Raumschiff, Ladung und BroadcastKommunikator.
 * 
 * @author LukasFuhlbruegge
 * @version 1.0
 * 
 */
public class Test {
	/**
	 * Effekt: Erstellt ein Anfangsszenario und testet die Methoden, die gew�nscht sind.
	 * Nutzt biszu alle Methoden. 
	 * @param args wird nicht verwendet
	 * 
	 */
	public static void main(String[] args) {
		BroadcastKommunikator broadcastKommunikator = new BroadcastKommunikator();
		// Klingonen initialisieren
		Ladung[] klingonenLadungen = new Ladung[2];
		Ladung ferengi_schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		klingonenLadungen[0] = ferengi_schneckensaft;
		Ladung klingonenschwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		klingonenLadungen[1] = klingonenschwert;
		Raumschiff klingonen = new Raumschiff(klingonenLadungen, "IKS Hegh'ta", 100, 100, 100, 100, 2, 1,
				broadcastKommunikator);

		// Romulaner initialisieren
		Ladung[] romulanerLadungen = new Ladung[3];
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		romulanerLadungen[0] = borgSchrott;
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		romulanerLadungen[1] = roteMaterie;
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		romulanerLadungen[2] = plasmaWaffe;
		Raumschiff romulaner = new Raumschiff(romulanerLadungen, "IRW Khazara", 100, 100, 100, 100, 2, 2,
				broadcastKommunikator);

		// Vulkanier initialisieren
		Ladung[] vulkanierLadungen = new Ladung[2];
		Ladung photonenTorpedos = new Ladung("Photonentorpedos", 3);
		vulkanierLadungen[0] = photonenTorpedos;
		Ladung forschungsSonden = new Ladung("Forschungssonde", 35);
		vulkanierLadungen[1] = forschungsSonden;
		Raumschiff vulkanier = new Raumschiff(vulkanierLadungen, "N'var", 40, 45, 100, 100, 5, 0,
				broadcastKommunikator);

		// Action
		klingonen.torpedoschuss(romulaner, 1,true);
		romulaner.phaserkanonenschuss(klingonen);
		vulkanier.sendeNachricht("Gewalt ist nicht logisch");
		klingonen.zustand();
		vulkanier.reparaturAndroidenEinsetzen(5, true, true);
		vulkanier.torpedoschuss(null, 3, false);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.torpedoschuss(romulaner, 2, true);
		klingonen.zustand();
		romulaner.zustand();
		vulkanier.zustand();
		System.out.println(broadcastKommunikator.getLog());
	}

}
