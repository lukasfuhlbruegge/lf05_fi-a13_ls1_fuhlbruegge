import java.util.Random;

/**
 * Datei: Raumschiff.java 
 * Aufgabe: Die Klasse modelliert ein Raumschiff.
 * 
 * @author LukasFuhlbruegge
 * @version 1.0
 * 
 */
public class Raumschiff {

	// Attribute
	private Ladung[] ladungsverzeichnis;
	private String name;
	private int huelle;
	private int schild;
	private int energieversorgung;
	private int lebenserhaltung;
	private int reperaturandroiden;
	private int photonentorpedos;
	private BroadcastKommunikator broadcastKommunikator;

	// Konstruktoren
	public Raumschiff(Ladung[] ladungsverzeichnis, String name, int huelle, int schild, int energieversorgung,
			int lebenserhaltung, int reperaturandroiden, int photonentorpedos,
			BroadcastKommunikator broadcastKommunikator) {
		super();
		this.ladungsverzeichnis = ladungsverzeichnis;
		this.name = name;
		this.huelle = huelle;
		this.schild = schild;
		this.energieversorgung = energieversorgung;
		this.lebenserhaltung = lebenserhaltung;
		this.reperaturandroiden = reperaturandroiden;
		this.photonentorpedos = photonentorpedos;
		this.broadcastKommunikator = broadcastKommunikator;
	}

	// Verwaltungsmethoden
	public Ladung[] getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(Ladung[] ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHuelle() {
		return huelle;
	}

	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	public int getSchild() {
		return schild;
	}

	public void setSchild(int schild) {
		this.schild = schild;
	}

	public int getEnergieversorgung() {
		return energieversorgung;
	}

	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	public int getLebenserhaltung() {
		return lebenserhaltung;
	}

	public void setLebenserhaltung(int lebenserhaltung) {
		this.lebenserhaltung = lebenserhaltung;
	}

	public int getReperaturandroiden() {
		return reperaturandroiden;
	}

	public void setReperaturandroiden(int reperaturandroiden) {
		this.reperaturandroiden = reperaturandroiden;
	}

	public int getPhotonentorpedos() {
		return photonentorpedos;
	}

	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}

	// allgemeine Methoden

	/**
	 * Effekt: Die angegebene Ladung wird dem Raumschiff hinzugefügt.
	 * 
	 * @param ladung die hinzuzufügende Ladung
	 */
	public void beladen(Ladung ladung) {
		for (int i = 0; i < ladungsverzeichnis.length; i++) {
			if (ladungsverzeichnis[i] == null) {
				ladungsverzeichnis[i] = ladung;
				break;
			}
		}
	}

	/**
	 * Effekt: Das Raumschiff schickt die gewünschte Anzahl Torpedos gegen ein
	 * ausgewähltes Raumschiff und zieht die Anzahl der Torpedos aus dem
	 * Ladungsverzeichnis oder den Kanonenrohren. Nutzt sendeNachricht() und
	 * treffer() aus Raumschiff
	 * 
	 * @param raumschiff Ziel der Torpedos
	 * @param anzahl     Anzahl der zu sendenden Torpedos
	 * @param feuern     Soll gefeuert werden?
	 */
	public void torpedoschuss(Raumschiff raumschiff, int anzahl, boolean feuern) {
		if (photonentorpedos < anzahl || photonentorpedos == 0) {
			for (int i = 0; i < ladungsverzeichnis.length; i++) {
				if (ladungsverzeichnis[i].getTyp() == "Photonentorpedos") {
					if (ladungsverzeichnis[i].getAnzahl() >= anzahl) {
						this.setPhotonentorpedos(anzahl);
						ladungsverzeichnis[i].setAnzahl(ladungsverzeichnis[i].getAnzahl() - anzahl);
					} else {
						setPhotonentorpedos(ladungsverzeichnis[i].getAnzahl());
						ladungsverzeichnis[i].setAnzahl(0);
					}
					break;
				}
			}
		}
		if (feuern) {
			if (getPhotonentorpedos() < 1) {
				sendeNachricht("-=*Click+=-");
			} else {
				setPhotonentorpedos(0);
				System.out.println(anzahl + " Photonentorpedo(s) eingesetzt.");
				sendeNachricht("Photonentorpedo abgeschossen");
				raumschiff.treffer();
			}
		}
	}

	/**
	 * Effekt: Schießt mit der Phaserkanone auf ein ausgewähltes Raumschiff. Ist die
	 * Energie zu gering wird "-=*Click*=-" ausgegeben. Nutzt treffer() und
	 * sendeNachricht aus Raumschigg
	 * 
	 * @param raumschiff Das anvisierte Raumschiff
	 */
	public void phaserkanonenschuss(Raumschiff raumschiff) {
		if (getEnergieversorgung() > 50) {
			sendeNachricht("Phaserkanone abgeschossen");
			raumschiff.treffer();
			setEnergieversorgung(getEnergieversorgung() - 50);
		} else {
			sendeNachricht("-=*Click*=-");
		}
	}

	/**
	 * Effekt: Simuliert den Schaden auf ein Raumschiff.
	 */
	public void treffer() {
		System.out.println(getName() + " wurde getroffen");
		if (getSchild() > 0 && getSchild() <= 50) {
			setSchild(0);
		} else if (getSchild() > 50) {
			setSchild((getSchild() - 50));
		}
		if (getSchild() == 0) {
			setHuelle(getHuelle() - 50);
			setLebenserhaltung(getLebenserhaltung() - 50);
		}
		if (getHuelle() <= 0) {
			sendeNachricht("Die Lebenserhaltungssysteme von " + getName() + " sind zerstört worden.");
			setHuelle(0);
		}
	}

	/**
	 * Effekt: Sendet eine Nachricht an die Konsole. Nutzt add() aus
	 * Broadcastkommunikator
	 * 
	 * @param nachricht Die Nachricht
	 */
	public void sendeNachricht(String nachricht) {
		System.out.println("Nachricht von " + getName() + ": " + nachricht);
		broadcastKommunikator.add(nachricht);
	}

	/**
	 * Effekt: Der Zustand des Raumschiffs wird auf die Konsole geprinted. Nutzt
	 * ladungsverzeichnisAusgeben( ) aus Konto.
	 * 
	 */
	public void zustand() {
		System.out.println();
		System.out.println("Das Raumschiff " + getName() + " hat die Huelle von " + getHuelle() + ", \ndas Schild von "
				+ getSchild() + ", die Energieversorgung von " + getEnergieversorgung() + ",\ndie Lebenserhaltung von "
				+ getLebenserhaltung() + ", " + getReperaturandroiden() + " Reperaturandroiden\nund "
				+ getPhotonentorpedos() + " Photonentorpedos.");
		System.out.println("Folgende Ladungen sind enthalten:");
		ladungsverzeichnisAusgeben();
	}

	/**
	 * Effekt: Das Ladungsverzeichnis wird ausgegeben.
	 * 
	 */
	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.length; i++) {
			if (ladungsverzeichnis[i] != null) {
				System.out.println(ladungsverzeichnis[i].toString());
			}
		}
		System.out.println();
	}

	/**
	 * Effekt: Es werden leere Ladngen aus dem Verzeichnis entfernt.
	 * 
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < ladungsverzeichnis.length; i++) {
			if (ladungsverzeichnis[i].getAnzahl() < 1) {
				ladungsverzeichnis[i] = null;
			}
		}
	}

	/**
	 * Effekt: Eine Ladung wird hinzugefügt. Nutzt beladen() aus Raumschiff.
	 * 
	 */
	public void addLadung(Ladung neueLadung) {
		beladen(neueLadung);
	}

	/**
	 * Effekt: Das Schiff wird repariert um einen Zufallswert.
	 * 
	 * @param anzahl Die Anzahl der einzusetzenden Reperaturandroiden.
	 * @param huelle Soll die Huelle repariert werden?
	 * @param schild Soll das Schild repariet werden?
	 */
	public void reparaturAndroidenEinsetzen(int anzahl, boolean huelle, boolean schild) {
		Random random = new Random();
		int zufall = random.nextInt(101) - 1;
		int zaehler = 0;
		if (huelle) {
			zaehler++;
		}
		if (schild) {
			zaehler++;
		}
		int reperatur = (zufall * anzahl) / zaehler;
		if (schild) {
			setSchild(getSchild() + reperatur);
		}
		if (huelle) {
			setHuelle(getHuelle() + reperatur);
		}
		if (getHuelle() > 100) {
			setHuelle(100);
		}
		if (getSchild() > 100) {
			setSchild(100);
		}
	}
}