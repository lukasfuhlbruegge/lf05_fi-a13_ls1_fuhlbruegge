import java.util.Scanner;

public class Arrayuebungen {
	static int[] zahlen=new int[10];
	static char[] palindrom=new char[5];
	public static void main(String[] args) {	
		aufgabe1();
		aufgabe2();
		aufgabe3();
	}
	public static void aufgabe1() {
		System.out.println("Aufgabe 1 Start");
		for(int i=0;i<10;i++) {
			zahlen[i]=i;
		}
		for (int j=0;j<10;j++) {
			System.out.println(zahlen[j]);
		}
		System.out.println("Aufgabe 1 Ende");
	}
	public static void aufgabe2() {
		System.out.println("Aufgabe 2 Start");
		for (int k=0; k<10;k++) {
			zahlen[k]=k*2+1;
		}
		for (int j=0;j<10;j++) {
				System.out.println(zahlen[j]);
		}
		System.out.println("Aufgabe 2 Ende");
	}
	public static void aufgabe3 () {
	    Scanner tastatur=new Scanner(System.in);
	    System.out.println("Aufgabe 3 Start");
	    for (int l=0; l<5;l++) {
	    	System.out.println("Bitte geben Sie ein Zeichen ein.");
	    	palindrom[l]=tastatur.next().charAt(0);
	    }
	    for (int m=4; m>=0; m--) {
	    	if (m>0) {
	    		System.out.print(palindrom[m]);
	    	}
	    	else {
	    		System.out.println(palindrom[m]);
	    	}
	    }
	    System.out.println("Aufgabe 3 Ende");
	}
}
