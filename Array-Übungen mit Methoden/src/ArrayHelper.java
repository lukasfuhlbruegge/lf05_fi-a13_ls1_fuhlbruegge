import java.util.Scanner;

public class ArrayHelper {
	static String s="";
	static int[] array = {7,4,3562, 9, 3, 143};
	static int[][] trans = {
		      {1, 2, 3, 4}, 
		      {2, 2, 3, 4}, 
		      {3, 3, 3, 4},
		      {4, 4, 4, 4},
		};
	public static void main(String[] args) {
		System.out.println("Der String ist: " + convertArraytoString(array) +".");
		aufgabe2(array);
		ausgabe(aufgabe2(array));
		ausgabe(aufgabe3(aufgabe2(array)));
		aufgabe4(5);
		ausgabe2d(aufgabe5(3,2));
		System.out.println(aufgabe6(trans));
		System.out.println(aufgabe6(a));
	}
	public static void ausgabe(int[] zahl) {
		for (int i=0;i<zahl.length;i++) {
			
			System.out.print(zahl[i] + " ");
		}
		System.out.println();
		System.out.println();
	}
	public static void ausgabe2d(int [][] zahl) {
		for (int i=0;i<zahl[0].length;i++) {
			for (int j=0; j<zahl.length;j++) {
			System.out.print(zahl[j][i] + " ");
			}
		System.out.println();
		}
	}
	
	public static String convertArraytoString(int[] zahlen) {
		for (int i=0; i<zahlen.length; i++) {
			if(i!=zahlen.length-1) {
			s= s+ zahlen[i] +", ";
			}
			else {
			s= s+zahlen[i];
			}
		}
		return s;
	}
	public static int[] aufgabe2(int[] zahlen) {
		for (int i=0; i<zahlen.length/2; i++) {
			int hilfe =zahlen[i];
			zahlen[i]=zahlen[zahlen.length-1-i];
			zahlen[zahlen.length-1-i]=hilfe;
		}
		return zahlen;
	}
	public static int[] aufgabe3(int zahlen[]) {
		for (int i=0; i<zahlen.length; i++) {
			int[] array=new int[zahlen.length];
			array[i]=zahlen[zahlen.length-1-i];
		}
		return array;
	}
	public static void aufgabe4(int zeilen) {
		double[][] table =new double[2][zeilen];
		for (int i=0; i<zeilen; i++) {
			table[0][i]=i*10.0;
			table[1][i]=(5.0/9.0)*(table[0][i]-32.0);
			System.out.println(table[0][i] + "|" + table[1][i]);
		}
	}
	public static int[][] aufgabe5(int zeilenanzahl, int spaltenanzahl) {
		Scanner keys=new Scanner(System.in);
		int[][] arr = new int[zeilenanzahl][spaltenanzahl];
		for (int i=0; i<zeilenanzahl; i++) {
			for (int j=0; j<spaltenanzahl;j++) {
				System.out.println("Bitte geben Sie den Wert f�r die "+ (j+1) + ". Zeile und die "+ (i+1) +". Spalte an.");
				arr[i][j]=keys.nextInt();
			}
		}
		return arr;
	}
	public static boolean aufgabe6(int [][] arr) {
		for (int i=0; i<arr[0].length;i++) {
			for(int j=0; j<arr.length-1;j++) {
				if (arr[i][j+1] != arr[j+1][i]) {
					return false;
				}
			}
		}
		return true;
	}
}
