package konsolenausgabe;

public class Aufgabe6 {

	public static void main(String[] args) {
		
		
		System.out.printf("%-12s" + "|" +"%10s\n", 	"Fahrenheit", "Celsius");
		System.out.printf("%.23s\n", "----------------------------------");
		System.out.printf("%-12s" + "|" +"%10s\n", 	"-20", "-28.89");
		System.out.printf("%-12s" + "|" +"%10s\n", 	"-10", "-23.33");
		System.out.printf("%-12s" + "|" +"%10s\n", 	"+0", "-17.78");
		System.out.printf("%-12s" + "|" +"%10s\n", 	"+20", "-6.67");
		System.out.printf("%-12s" + "|" +"%10s\n", 	"+30", "-1.11");
		
	}

}
